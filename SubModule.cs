﻿using HarmonyLib;
using System;
using System.Windows.Forms;
using TaleWorlds.CampaignSystem;
using TaleWorlds.Core;
using TaleWorlds.MountAndBlade;
using TaleWorlds.InputSystem;

namespace Transmog
{
	public class SubModule : MBSubModuleBase
	{
		protected override void OnSubModuleLoad()
		{
			base.OnSubModuleLoad();
			try
			{
				var harmony = new Harmony("mod.transmog.bannerlord");
				harmony.PatchAll();
			}
			catch (Exception e)
			{
				MessageBox.Show("Couldn't apply Harmony due to: " + Utils.FlattenException(e));
			}
		}

		protected override void OnGameStart(Game game, IGameStarter gameStarterObject)
		{
			try
			{
				base.OnGameStart(game, gameStarterObject);
				if (!(game.GameType is Campaign))
				{
					return;
				}
				if (gameStarterObject != null)
				{
					CampaignGameStarter campaign = gameStarterObject as CampaignGameStarter;
					campaign.AddBehavior(TransmogBehavior.Instance);
				}
			}
			catch (Exception e)
			{
				MessageBox.Show(Utils.FlattenException(e));
			}
		}

		public override void OnCampaignStart(Game game, object starterObject)
		{
			TransmogBehavior.Initialize();
		}
	}
}