﻿using HarmonyLib;
using SandBox.GauntletUI;
using System;
using System.Collections.Generic;
using System.Linq;
using TaleWorlds.CampaignSystem;
using TaleWorlds.CampaignSystem.ViewModelCollection;
using TaleWorlds.Core;
using TaleWorlds.Engine.Screens;
using TaleWorlds.InputSystem;
using TaleWorlds.Library;

namespace Transmog
{
    class TransmogInventoryScreen
    {
        public static ItemObject clipboard_item;

        [HarmonyPatch(typeof(InventoryGauntletScreen), "OnFrameTick")]
        class InventoryGauntletScreen_Patch
        {
            static bool Prefix()
            {
                if (InputKey.LeftControl.IsDown())
                    return false;
                return true;
            }
            static void Postfix(InventoryGauntletScreen __instance)
            {
                if (InputKey.C.IsPressed() && InputKey.LeftControl.IsDown() && !isTrading())
                {
                    SPItemVM selected_SPItemVM = GetSelectedItemVM(GetSPInventoryVM(__instance));
                    if (selected_SPItemVM != null)
                        TransmogCopy.SelectedItemCopy(selected_SPItemVM.ItemRosterElement.EquipmentElement.Item);
                    else
                        TransmogCopy.EquipmentInventoryCopy();
                }
                else if (InputKey.V.IsPressed() && InputKey.LeftControl.IsDown() && !isTrading())
                {
                    SPItemVM selected_SPItemVM = GetSelectedItemVM(GetSPInventoryVM(__instance));
                    if (selected_SPItemVM != null)
                    {
                        if (selected_SPItemVM.InventorySide == InventoryLogic.InventorySide.PlayerInventory)
                            TransmogPaste.SelectedItemPaste(selected_SPItemVM);
                        else if (selected_SPItemVM.InventorySide == InventoryLogic.InventorySide.Equipment)
                            TransmogPaste.SelectedEquipmentPaste(selected_SPItemVM);
                    }
                    else
                        TransmogPaste.EquipmentPaste(System.Windows.Forms.Clipboard.GetText());
                }
                else if (InputKey.H.IsPressed() && InputKey.LeftControl.IsDown() && !isTrading())
                {
                    SPItemVM selected_SPItemVM = GetSelectedItemVM(GetSPInventoryVM(__instance));
                    if (selected_SPItemVM.InventorySide == InventoryLogic.InventorySide.PlayerInventory)
                        TransmogReset.SelectedItemHide(selected_SPItemVM);
                    else if (selected_SPItemVM.InventorySide == InventoryLogic.InventorySide.Equipment)
                        TransmogReset.SelectedEquipmentHide(selected_SPItemVM);
                }
                else if (InputKey.R.IsPressed() && !isTrading())
                {
                    if (InputKey.LeftControl.IsDown() && !InputKey.LeftShift.IsDown())
                    {
                        SPItemVM selected_SPItemVM = GetSelectedItemVM(GetSPInventoryVM(__instance));
                        if (selected_SPItemVM != null)
                        {
                            if (selected_SPItemVM.InventorySide == InventoryLogic.InventorySide.PlayerInventory)
                                TransmogReset.SelectedItemReset(selected_SPItemVM);
                            else if (selected_SPItemVM.InventorySide == InventoryLogic.InventorySide.Equipment)
                                TransmogReset.SelectedEquipmentReset(selected_SPItemVM);
                        }
                        else
                            TransmogReset.EquipmentReset();
                    }
                    //else if (InputKey.LeftControl.IsDown() && InputKey.LeftShift.IsDown())
                    //{
                    //    TransmogReset.ResetAllTransmogs();
                    //    InventoryManager im = Traverse.Create(Campaign.Current).Field<InventoryManager>("_inventoryManager").Value;
                    //    im.CloseInventoryPresentation();
                    //}
                }
            }
        }

        static bool isTrading()
        {
            InventoryLogic logic = GetInventoryLogic();
            if ((logic.GetBoughtItems().Count != 0 || logic.GetSoldItems().Count != 0) && logic.IsTrading)
            {
                InformationManager.DisplayMessage(new InformationMessage("Transmog not possible while a trade is pending.", Colors.Yellow));
                return true;
            }
            return false;
        }

        public static SPItemVM GetEquipmentSPItemVM(ItemObject.ItemTypeEnum item_type)
        {
            SPInventoryVM vm = GetSPInventoryVM();
            SPItemVM equipped_itemVM = null;
            if (item_type == ItemObject.ItemTypeEnum.HeadArmor)
                equipped_itemVM = vm.CharacterHelmSlot;
            else if (item_type == ItemObject.ItemTypeEnum.Cape)
                equipped_itemVM = vm.CharacterCloakSlot;
            else if (item_type == ItemObject.ItemTypeEnum.BodyArmor)
                equipped_itemVM = vm.CharacterTorsoSlot;
            else if (item_type == ItemObject.ItemTypeEnum.HandArmor)
                equipped_itemVM = vm.CharacterGloveSlot;
            else if (item_type == ItemObject.ItemTypeEnum.LegArmor)
                equipped_itemVM = vm.CharacterBootSlot;
            else if (item_type == ItemObject.ItemTypeEnum.HorseHarness)
                equipped_itemVM = vm.CharacterMountArmorSlot;
            return equipped_itemVM;
        }

        public static SPInventoryVM GetSPInventoryVM(InventoryGauntletScreen igs = null)
        {
            if (igs == null)
            {
                ScreenBase topScreen = ScreenManager.TopScreen;
                if (topScreen is InventoryGauntletScreen)
                    return Traverse.Create(topScreen).Field<SPInventoryVM>("_dataSource").Value;
                else
                    return null;
            }
            return Traverse.Create(igs).Field<SPInventoryVM>("_dataSource").Value;
        }

        public static SPItemVM GetSelectedItemVM(SPInventoryVM vm = null)
        {
            SPItemVM item = null;
            if (vm == null)
            {
                vm = GetSPInventoryVM();
                if (vm == null)
                {
                    InformationManager.DisplayMessage(new InformationMessage("Transmog error: Cannot detect SPInventoryVM", Colors.Red));
                    return item;
                }
            }
            item = Traverse.Create(vm).Field<SPItemVM>("_selectedItem").Value;
            return item;
        }

        public static InventoryLogic GetInventoryLogic(SPInventoryVM vm = null)
        {
            if (vm == null)
            {
                vm = GetSPInventoryVM();
                if (vm == null)
                    return null;
            }
            return Traverse.Create(vm).Field<InventoryLogic>("_inventoryLogic").Value;
        }
    }

    public static class ExtensionMethods
    {

        public static void ReplaceSPItemVM(this SPItemVM clicked_itemVM, EquipmentElement new_element)
        {
            ScreenBase topScreen = ScreenManager.TopScreen;
            if (topScreen is InventoryGauntletScreen)
            {
                EquipmentElement remove_element = clicked_itemVM.ItemRosterElement.EquipmentElement;
                SPInventoryVM vm = Traverse.Create(topScreen).Field<SPInventoryVM>("_dataSource").Value;
                InventoryLogic logic = Traverse.Create(vm).Field<InventoryLogic>("_inventoryLogic").Value;
                ItemRoster[] rosters = Traverse.Create(logic).Field<ItemRoster[]>("_rosters").Value;
                List<TransferCommandResult> list = new List<TransferCommandResult>();
                InventoryLogic.InventorySide inventorySide = InventoryLogic.InventorySide.PlayerInventory;

                ItemRosterElement elementCopyAtIndex = clicked_itemVM.ItemRosterElement;
                list.Add(new TransferCommandResult(InventoryLogic.InventorySide.PlayerInventory, elementCopyAtIndex, -1, elementCopyAtIndex.Amount-1, EquipmentIndex.None, null, elementCopyAtIndex.EquipmentElement.Item.IsCivilian));
                rosters[(int)inventorySide].AddToCounts(remove_element, -1);

                int add_index = rosters[(int)inventorySide].AddToCounts(new_element, 1);
                ItemRosterElement elementCopyAtIndex2 = rosters[(int)inventorySide].GetElementCopyAtIndex(add_index);
                list.Add(new TransferCommandResult(InventoryLogic.InventorySide.PlayerInventory, elementCopyAtIndex2, 1, elementCopyAtIndex2.Amount, EquipmentIndex.None, null, elementCopyAtIndex2.EquipmentElement.Item.IsCivilian));


                Traverse.Create(logic).Method("SetCurrentStateAsInitial", new Type[] { }).GetValue();
                Traverse.Create(logic).Method("OnAfterTransfer", new Type[] { typeof(List<TransferCommandResult>) }).GetValue(list);

                List<SPItemVM> list2 = vm.RightItemListVM.ToList<SPItemVM>();
                if (list2[list2.Count - 1].ItemRosterElement.EquipmentElement.Item.Equals(new_element.Item))
                {
                    for (int i = list2.Count - 1; i >= 0; i--)
                    {
                        if (list2[i].Equals(clicked_itemVM))
                        {
                            vm.RightItemListVM.Insert(i + 1, list2[list2.Count - 1]);
                            vm.RightItemListVM.RemoveAt(list2.Count);
                        }
                    }
                }
                //rosters[(int)inventorySide].RemoveZeroCounts();
                Traverse.Create<ItemRoster>().Method("RemoveZeroCountsFromRoster", new Type[] { typeof(ItemRoster), }).GetValue(rosters[(int)inventorySide]);
                Traverse.Create(vm).Method("ExecuteRemoveZeroCounts", new Type[] { }).GetValue();
            }
        }


    }
}
