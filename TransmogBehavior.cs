﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using TaleWorlds.CampaignSystem;
using TaleWorlds.Core;
using TaleWorlds.SaveSystem;

namespace Transmog
{
    class TransmogBehavior : CampaignBehaviorBase
    {
        public static readonly TransmogBehavior Instance = new TransmogBehavior();
        public static Dictionary<ItemObject, ItemObject> transmogs_mesh; //Transmog_item first, mesh_item second
        public static Dictionary<ItemObject, ItemObject> transmogs_stat; //Transmog_item first, stat_item second
        public override void RegisterEvents()
        {
            //CampaignEvents.start.AddNonSerializedListener(this, new Action(this.BeforeSaveEvent));
        }

        public static void Initialize()
        {
            if (transmogs_mesh == null)
            {
                transmogs_mesh = new Dictionary<ItemObject, ItemObject>();
                transmogs_stat = new Dictionary<ItemObject, ItemObject>();
            }
        }

        public override void SyncData(IDataStore dataStore)
        {
            try
            {
                if (dataStore.IsLoading)
                {
                    transmogs_mesh = new Dictionary<ItemObject, ItemObject>();
                    transmogs_stat = new Dictionary<ItemObject, ItemObject>();
                    dataStore.SyncData<Dictionary<ItemObject, ItemObject>>("transmogs_mesh", ref transmogs_mesh);
                    dataStore.SyncData<Dictionary<ItemObject, ItemObject>>("transmogs_stat", ref transmogs_stat);
                    foreach (ItemObject transmog_item in transmogs_mesh.Keys.ToList())
                    {
                        // REPAIR OLD SAVEGAME START
                        //ItemObject stat_item = transmogs_stat[transmog_item];
                        //ItemObject mesh_item = transmogs_mesh[transmog_item];
                        //while (stat_item.StringId.StartsWith("TMOG_"))
                        //    stat_item = TransmogBehavior.transmogs_stat[stat_item]; // stat_item is a transmogrified item, grab its original stat_item
                        //while (mesh_item.StringId.StartsWith("TMOG_"))
                        //    mesh_item = TransmogBehavior.transmogs_mesh[mesh_item]; // mesh_item is a transmogrified item, grab its original mesh_item
                        //transmogs_mesh[transmog_item] = mesh_item;
                        //transmogs_stat[transmog_item] = stat_item;
                        // REPAIR OLD SAVEGAME END

                        if (transmog_item != null && !transmogs_stat.ContainsKey(transmog_item))
                        {
                            if (transmogs_stat.ContainsKey(transmog_item))
                                transmogs_stat.Remove(transmog_item);
                            if (transmogs_mesh.ContainsKey(transmog_item))
                                transmogs_mesh.Remove(transmog_item);
                        }
                        else if (transmog_item != null)
                        {
                            Transmogrifier.CopyStats(transmog_item, transmogs_stat[transmog_item], transmogs_mesh[transmog_item]);
                            transmog_item.AfterInitialized(); // Prevents game making the items Trash
                        }
                        //transmog_item.BeforeLoad(null); //    NECESSARY OR NOT?
                    }
                } else if (dataStore.IsSaving)
                {
                    dataStore.SyncData<Dictionary<ItemObject, ItemObject>>("transmogs_mesh", ref transmogs_mesh);
                    dataStore.SyncData<Dictionary<ItemObject, ItemObject>>("transmogs_stat", ref transmogs_stat);
                    //MessageBox.Show("transmogs_mesh count: " + transmogs_mesh.Count);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Utils.FlattenException(e));
                if (dataStore.IsLoading && MessageBox.Show("Transmog mod has encountered a problem, do you want to try reseting ALL transmog items to try to fix your savegame? Press Ok to reset, cancel to ignore.")
                    == DialogResult.OK)
                {
                    TransmogReset.ResetAllTransmogs();
                }
            }
        }

        public static void RegisterTransmogItem(ItemObject transmog_item, ItemObject stat_item, ItemObject mesh_item)
        {
            if (transmogs_mesh == null)
                Initialize();
            transmogs_mesh[transmog_item] = mesh_item;
            transmogs_stat[transmog_item] = stat_item;
        }

        public class MySaveDefiner : SaveableTypeDefiner
        {
            public MySaveDefiner() : base(568947283)
            {
            }

            protected override void DefineContainerDefinitions()
            {
                ConstructContainerDefinition(typeof(Dictionary<ItemObject, ItemObject>));
            }
        }
    }
}
