﻿using MCM.Abstractions.Attributes;
using MCM.Abstractions.Attributes.v2;
using MCM.Abstractions.Settings.Base.Global;
using MCM.Abstractions.Settings.Base.PerCharacter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaleWorlds.CampaignSystem;

namespace Transmog
{
    internal sealed class MCMUISettings : AttributeGlobalSettings<MCMUISettings> // AttributePerCharacterSettings<MCMUISettings>
    {
        public override string Id => "Transmog";
        public override string DisplayName => "Transmog - Cosmetic Equipment";
        public override string FolderName => "Transmog";
        public override string Format => "xml";

        [SettingPropertyBool("RESET", RequireRestart = false, HintText = "Only accessible from the campaign map, load a savegame first!")]
        [SettingPropertyGroup("Reset Options", IsMainToggle = true)]
        public bool IsResetEnabled
        {
            get => Campaign.Current != null;
            set { }
        }

        [SettingPropertyBool("Revert ALL transmogrified items", RequireRestart = false, HintText = "MAKE A NEW SAVE after enabling the option above before disabling the Transmog mod in your mod launcher! Otherwise your savegame will not load after uninstallation.")]
        [SettingPropertyGroup("Reset Options")]
        public bool ResetMod
        {
            get => false;
            set
            {
                TransmogReset.ResetAllTransmogs();
            }
        }

        [SettingPropertyBool("Clear all unused transmogrified items", RequireRestart = false, HintText = "Create a backup save before using just in case! Delete all records of non-existing transmog items. You can use this to combat savegame bloat.")]
        [SettingPropertyGroup("Reset Options")]
        public bool DeleteUnusedMod
        {
            get => false;
            set
            {
                TransmogReset.DeleteUnusedTransmogs();
            }
        }

        [SettingPropertyBool("Clear all unequiped transmogrified items", RequireRestart = false, HintText = "Create a backup save before using just in case! Revert/delete all records of transmog items that are NOT currently equipped by characters. You can use this to combat savegame bloat.")]
        [SettingPropertyGroup("Reset Options")]
        public bool DeleteUnequipedMod
        {
            get => false;
            set
            {
                TransmogReset.DeleteUnusedTransmogs(true);
            }
        }

        [SettingPropertyInteger("Item Tier Difference Allowed", 0, 6, RequireRestart = false, HintText = "With a value of 1, the item to be transmogrified may not be more than 1 tier lower than the item from which the visual is to be copied. At 6, no restrictions apply.")]
        [SettingPropertyGroup("General", GroupOrder = 0)]
        public int TransmogItemTierDifferenceAllowed
        {
            get => Transmogrifier.TransmogItemTierDifferenceAllowed;
            set
            {
                Transmogrifier.TransmogItemTierDifferenceAllowed = value;
            }
        }
    }

}
