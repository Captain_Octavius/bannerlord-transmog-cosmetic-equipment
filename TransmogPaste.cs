﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using TaleWorlds.CampaignSystem;
using TaleWorlds.CampaignSystem.ViewModelCollection;
using TaleWorlds.Core;
using TaleWorlds.Core.ViewModelCollection;
using TaleWorlds.Library;

namespace Transmog
{
    class TransmogPaste
    {
        public static void SelectedItemPaste(SPItemVM selected_SPItemVM, string clipboard_substitute = null)
        {
            XElement xml;
            try
            {
                if (clipboard_substitute == null)
                    xml = XElement.Parse(System.Windows.Forms.Clipboard.GetText());
                else
                    xml = XElement.Parse(clipboard_substitute);
            }
            catch
            {
                InformationManager.DisplayMessage(new InformationMessage("Invalid data for transmogrification!", Colors.Red));
                return;
            }
            EquipmentElement selected_element = selected_SPItemVM.ItemRosterElement.EquipmentElement;
            string item_type = selected_element.Item.ItemType.ToString();
            if (xml.Attribute(item_type) != null)
            {
                string mesh_item_stringid = xml.Attribute(item_type).Value;
                ItemObject mesh_item;
                if (mesh_item_stringid != "hidden")
                {
                    if (mesh_item_stringid != TransmogInventoryScreen.clipboard_item?.StringId)
                    {
                        TransmogInventoryScreen.clipboard_item = ItemObject.All.FirstOrDefault(item => item.StringId == mesh_item_stringid);
                        if (TransmogInventoryScreen.clipboard_item == null)
                        {
                            InformationManager.DisplayMessage(new InformationMessage("No item with StringID " + mesh_item_stringid + "exists!", Colors.Red));
                            return;
                        }
                    }
                    mesh_item = TransmogInventoryScreen.clipboard_item;
                } else
                    mesh_item = selected_element.Item;
                ItemObject stat_item = selected_element.Item;
                ItemObject transmog_item = Transmogrifier.TransmogItem(stat_item, mesh_item, mesh_item_stringid == "hidden");
                if (transmog_item != null)
                {
                    EquipmentElement transmog_element = new EquipmentElement(transmog_item, selected_element.ItemModifier);
                    selected_SPItemVM.ReplaceSPItemVM(transmog_element);
                    //InformationManager.DisplayMessage(new InformationMessage("Transmog paste: " + selected_element.Item.GetName(), Colors.Green));
                }
            }
            else
                InformationManager.DisplayMessage(new InformationMessage("Transmog data does not contain " + item_type, Colors.Yellow));
        }

        public static void SelectedEquipmentPaste(SPItemVM item)
        {
            XElement xml;
            try
            {
                xml = XElement.Parse(System.Windows.Forms.Clipboard.GetText());
            }
            catch
            {
                InformationManager.DisplayMessage(new InformationMessage("Invalid data for transmogrification!", Colors.Red));
                return;
            }
            if (xml.Name == "TransmogData")
            {
                string item_type = item.ItemRosterElement.EquipmentElement.Item.ItemType.ToString();
                if (xml.Attribute(item_type) != null)
                {
                    XElement element = new XElement("TransmogData");
                    element.Add(xml.Attribute(item_type));
                    EquipmentPaste(element.ToString());
                }
            }
            else
                InformationManager.DisplayMessage(new InformationMessage("Invalid data for transmogrification!", Colors.Red));
        }

        public static void EquipmentPaste(string xml_data)
        {
            XElement xml;
            try
            {
                xml = XElement.Parse(xml_data);
            }
            catch
            {
                InformationManager.DisplayMessage(new InformationMessage("Invalid data for transmogrification!", Colors.Red));
                return;
            }
            if (xml.Name == "TransmogData")
            {
                SPInventoryVM vm = TransmogInventoryScreen.GetSPInventoryVM();
                InventoryLogic logic = TransmogInventoryScreen.GetInventoryLogic(vm);
                List<TransferCommand> transfer_list = new List<TransferCommand>();
                foreach (XAttribute attribute in xml.Attributes())
                {
                    if (Enum.TryParse<ItemObject.ItemTypeEnum>(attribute.Name.ToString(), out ItemObject.ItemTypeEnum item_type))
                    {
                        SPItemVM equipped_itemVM = TransmogInventoryScreen.GetEquipmentSPItemVM(item_type);
                        EquipmentIndex equipment_index = equipped_itemVM.ItemType;
                        EquipmentElement stat_element = equipped_itemVM.ItemRosterElement.EquipmentElement;
                        ItemObject stat_item = stat_element.Item;
                        ItemObject original_item = stat_item;
                        ItemObject mesh_item;
                        if (attribute.Value != "hidden")
                            mesh_item = ItemObject.All.FirstOrDefault(item => item.StringId == attribute.Value);
                        else
                            mesh_item = stat_item;
                        if (mesh_item != null && mesh_item.ItemType == item_type)
                        {
                            if (stat_item != null)
                            {
                                //stat_item = Transmogrifier.FindOriginal(stat_item, ref TransmogBehavior.transmogs_stat); // I don't think this one is necessary anymore?
                                ItemObject transmog_item = Transmogrifier.TransmogItem(stat_item, mesh_item, attribute.Value == "hidden");
                                if (transmog_item != null)
                                {
                                    EquipmentElement transmog_element = new EquipmentElement(transmog_item, stat_element.ItemModifier);
                                    MBBindingList<SPItemVM> list = Traverse.Create(vm).Field<MBBindingList<SPItemVM>>("_rightItemListVM").Value;
                                    Traverse.Create(vm).Method("ProcessUnequipItem", new Type[] { typeof(ItemVM) }).GetValue(equipped_itemVM);
                                    //Unequiped item
                                    SPItemVM original_item_vm = list.FirstOrDefault(item => item.ItemRosterElement.EquipmentElement.Item == original_item && item.ItemRosterElement.EquipmentElement.ItemModifier == stat_element.ItemModifier);
                                    original_item_vm.ReplaceSPItemVM(transmog_element);
                                    SPItemVM transmog_item_vm = list.FirstOrDefault(item => item.ItemRosterElement.EquipmentElement.Item == transmog_item && item.ItemRosterElement.EquipmentElement.ItemModifier == transmog_element.ItemModifier);
                                    //Item replaced with transmoged item
                                    ItemRoster[] rosters = Traverse.Create(logic).Field<ItemRoster[]>("_rosters").Value;
                                    InventoryLogic.InventorySide inventorySide = InventoryLogic.InventorySide.PlayerInventory;
                                    ItemRosterElement transmog_roster_element = rosters[(int)inventorySide].GetElementCopyAtIndex(rosters[(int)inventorySide].FindIndexOfElement(transmog_element));
                                    CharacterObject _currentCharacter = Traverse.Create(vm).Field<CharacterObject>("_currentCharacter").Value;
                                    transfer_list.Add(TransferCommand.Transfer(1, InventoryLogic.InventorySide.PlayerInventory, InventoryLogic.InventorySide.Equipment, transmog_roster_element, equipment_index, equipment_index, _currentCharacter, !vm.IsInWarSet));
                                    //Transmoged item equip transfer added to command list
                                    //InformationManager.DisplayMessage(new InformationMessage("Transmog: " + stat_element.GetModifiedItemName(), Colors.Green));
                                }
                            }
                        }
                    }
                }
                if (transfer_list.Count > 0)
                {
                    vm.IsRefreshed = false;
                    logic.AddTransferCommands(transfer_list);
                    Traverse.Create(vm).Method("RefreshInformationValues", new Type[] { }).GetValue();
                    vm.IsRefreshed = true;
                    ItemRoster[] rosters = Traverse.Create(logic).Field<ItemRoster[]>("_rosters").Value;
                    //rosters[(int)InventoryLogic.InventorySide.PlayerInventory].RemoveZeroCounts();
                    Traverse.Create<ItemRoster>().Method("RemoveZeroCountsFromRoster", new Type[] { typeof(ItemRoster), }).GetValue(rosters[(int)InventoryLogic.InventorySide.PlayerInventory]);
                    Traverse.Create(vm).Method("ExecuteRemoveZeroCounts", new Type[] { }).GetValue();
                    Traverse.Create(logic).Method("SetCurrentStateAsInitial", new Type[] { }).GetValue();
                }
                else
                    InformationManager.DisplayMessage(new InformationMessage("There is nothing to transmogrify.", Colors.Yellow));
            }
            else
                InformationManager.DisplayMessage(new InformationMessage("Invalid data for transmogrification!", Colors.Red));
        }

    }
}
