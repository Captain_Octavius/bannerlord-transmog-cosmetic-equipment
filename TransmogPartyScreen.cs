﻿using HarmonyLib;
using SandBox.GauntletUI;
using System.Linq;
using TaleWorlds.CampaignSystem;
using TaleWorlds.CampaignSystem.ViewModelCollection;
using TaleWorlds.Core;
using TaleWorlds.InputSystem;
using TaleWorlds.Library;

namespace Transmog
{
    class TransmogPartyScreen
    {
        [HarmonyPatch(typeof(GauntletPartyScreen), "OnFrameTick")]
        class InventoryGauntletScreen_Patch
        {
            static bool Prefix()
            {
                if (InputKey.LeftControl.IsDown())
                    return false;
                return true;
            }
            static CharacterObject last_character;
            static int equipment_index = 0;
            static void Postfix(InventoryGauntletScreen __instance)
            {
                if (InputKey.C.IsPressed() && InputKey.LeftControl.IsDown())
                {
                    PartyVM vm = Traverse.Create(__instance).Field<PartyVM>("_dataSource").Value;
                    CharacterObject current_character = vm.CurrentCharacter.Character;
                    if (current_character == last_character)
                    {
                        if (equipment_index < current_character.BattleEquipments.Count() - 1)
                            equipment_index++;
                        else
                            equipment_index = 0;
                    }
                    else
                    {
                        equipment_index = 0;
                        last_character = current_character;
                    }
                    TransmogCopy.EquipmentPartyCopy(current_character.BattleEquipments.ToList()[equipment_index]);
                    InformationManager.DisplayMessage(new InformationMessage("Transmog equipment variant " + (equipment_index+1) + " copied!", Colors.Green));
                }
            }
        }
    }
}
