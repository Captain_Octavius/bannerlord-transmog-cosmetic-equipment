﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using TaleWorlds.CampaignSystem;
using TaleWorlds.CampaignSystem.ViewModelCollection;
using TaleWorlds.Core;
using TaleWorlds.Library;

namespace Transmog
{
    class TransmogReset
    {

        public static void SelectedItemReset(SPItemVM selected_SPItemVM)
        {
            EquipmentElement selected_element = selected_SPItemVM.ItemRosterElement.EquipmentElement;
            ItemObject transmog_item = selected_element.Item;
            if (transmog_item.StringId.StartsWith("TMOG_"))
            {
                ItemObject original_item = TransmogBehavior.transmogs_stat[transmog_item];
                EquipmentElement original_element = new EquipmentElement(original_item, selected_element.ItemModifier);
                selected_SPItemVM.ReplaceSPItemVM(original_element);
                InformationManager.DisplayMessage(new InformationMessage("Transmog revert: " + original_item.Name.ToString(), Colors.Green));
            }
            else
                InformationManager.DisplayMessage(new InformationMessage("This is not a transmogrified item.", Colors.Yellow));
        }

        public static void SelectedEquipmentReset(SPItemVM selected_SPItemVM)
        {
            EquipmentElement selected_element = selected_SPItemVM.ItemRosterElement.EquipmentElement;
            ItemObject transmog_item = selected_element.Item;
            if (transmog_item.StringId.StartsWith("TMOG_"))
            {
                XElement element = new XElement("TransmogData");
                element.Add(new XAttribute(transmog_item.ItemType.ToString(), TransmogBehavior.transmogs_stat[transmog_item].StringId));
                TransmogPaste.EquipmentPaste(element.ToString());
                // Give command to transmog item's original mesh, Transmogrifier will revert item to original
            }
        }

        public static void SelectedItemHide(SPItemVM selected_SPItemVM)
        {
            EquipmentElement selected_element = selected_SPItemVM.ItemRosterElement.EquipmentElement;
            ItemObject transmog_item = selected_element.Item;
            if (!Transmogrifier.ValidateItems(transmog_item, transmog_item))
                return;
            if (transmog_item.StringId.StartsWith("TMOG_") && TransmogBehavior.transmogs_stat[transmog_item] == TransmogBehavior.transmogs_mesh[transmog_item])
                SelectedItemReset(selected_SPItemVM);
            else
            {
                XElement element = new XElement("TransmogData");
                element.Add(new XAttribute(transmog_item.ItemType.ToString(), "hidden"));
                TransmogPaste.SelectedItemPaste(selected_SPItemVM, element.ToString());
                // Give command to transmog item, Transmogrifier will hide the mesh
            }
        }

        public static void SelectedEquipmentHide(SPItemVM selected_SPItemVM)
        {
            EquipmentElement selected_element = selected_SPItemVM.ItemRosterElement.EquipmentElement;
            ItemObject transmog_item = selected_element.Item;
            if (!Transmogrifier.ValidateItems(transmog_item, transmog_item))
                return;
            if (transmog_item.StringId.StartsWith("TMOG_") && TransmogBehavior.transmogs_stat[transmog_item] == TransmogBehavior.transmogs_mesh[transmog_item])
                SelectedEquipmentReset(selected_SPItemVM);
            else
            {
                XElement element = new XElement("TransmogData");
                element.Add(new XAttribute(transmog_item.ItemType.ToString(), "hidden"));
                TransmogPaste.EquipmentPaste(element.ToString());
                // Give command to transmog item, Transmogrifier will hide the mesh
            }
        }

        public static void EquipmentReset()
        {
            SPInventoryVM vm = TransmogInventoryScreen.GetSPInventoryVM();
            XElement element = new XElement("TransmogData");
            XmlAddReset(element, vm.CharacterHelmSlot);
            XmlAddReset(element, vm.CharacterCloakSlot);
            XmlAddReset(element, vm.CharacterTorsoSlot);
            XmlAddReset(element, vm.CharacterGloveSlot);
            XmlAddReset(element, vm.CharacterBootSlot);
            XmlAddReset(element, vm.CharacterMountArmorSlot);
            TransmogPaste.EquipmentPaste(element.ToString());
            //System.Windows.Forms.Clipboard.SetText(element.ToString());
            //InformationManager.DisplayMessage(new InformationMessage("Transmog equipment copied!", Colors.Green));
        }

        static void XmlAddReset(XElement element, SPItemVM item_vm)
        {
            ItemObject item = item_vm.ItemRosterElement.EquipmentElement.Item;
            if (item != null && item.StringId.StartsWith("TMOG_"))
            {
                item = Transmogrifier.FindOriginal(item, ref TransmogBehavior.transmogs_stat);
                element.Add(new XAttribute(item.ItemType.ToString(), item.StringId));
            }
        }

        public static void DeleteUnusedTransmogs(bool reset_inventories = false)
        {
            HashSet<ItemObject> used_transmogs = new HashSet<ItemObject>();
            foreach (Hero hero in Campaign.Current.Heroes)
                FindTransmogItems(hero, ref used_transmogs);
            if (!reset_inventories)
            {
                foreach (PartyBase party in Campaign.Current.Parties)
                    FindTransmogItems(party.ItemRoster, ref used_transmogs);
                foreach (Settlement settlement in Campaign.Current.Settlements)
                    FindTransmogItems(settlement.Stash, ref used_transmogs);
            }
            else
            {
                foreach (PartyBase party in Campaign.Current.Parties)
                    ReplaceTransmogItems(party.ItemRoster, used_transmogs);
                foreach (Settlement settlement in Campaign.Current.Settlements)
                    ReplaceTransmogItems(settlement.Stash, used_transmogs);
            }
            int num_deleted = 0;
            foreach (ItemObject transmog in TransmogBehavior.transmogs_mesh.Keys.ToList())
            {
                if (!used_transmogs.Contains(transmog))
                {
                    TransmogBehavior.transmogs_mesh.Remove(transmog);
                    TransmogBehavior.transmogs_stat.Remove(transmog);
                    num_deleted++;
                    if (Traverse.Create(transmog).Property<bool>("isRegistered").Value)
                        Game.Current.ObjectManager.UnregisterObject(transmog);
                }
            }
            if (!reset_inventories)
                InformationManager.DisplayMessage(new InformationMessage("Number of unused transmog items removed: " + num_deleted, Colors.Yellow));
            else
                InformationManager.DisplayMessage(new InformationMessage("Number of unworn transmog items removed: " + num_deleted, Colors.Yellow));
        }


        public static void ResetAllTransmogs()
        {
            foreach (PartyBase party in Campaign.Current.Parties)
                ReplaceTransmogItems(party.ItemRoster);
            foreach (Settlement settlement in Campaign.Current.Settlements)
                ReplaceTransmogItems(settlement.Stash);
            foreach (Hero hero in Campaign.Current.Heroes)
            {
                ReplaceTransmogEquipment(hero.BattleEquipment, EquipmentIndex.Body);
                ReplaceTransmogEquipment(hero.BattleEquipment, EquipmentIndex.Cape);
                ReplaceTransmogEquipment(hero.BattleEquipment, EquipmentIndex.Gloves);
                ReplaceTransmogEquipment(hero.BattleEquipment, EquipmentIndex.Head);
                ReplaceTransmogEquipment(hero.BattleEquipment, EquipmentIndex.Leg);
                ReplaceTransmogEquipment(hero.BattleEquipment, EquipmentIndex.HorseHarness);
                ReplaceTransmogEquipment(hero.CivilianEquipment, EquipmentIndex.Body);
                ReplaceTransmogEquipment(hero.CivilianEquipment, EquipmentIndex.Cape);
                ReplaceTransmogEquipment(hero.CivilianEquipment, EquipmentIndex.Gloves);
                ReplaceTransmogEquipment(hero.CivilianEquipment, EquipmentIndex.Head);
                ReplaceTransmogEquipment(hero.CivilianEquipment, EquipmentIndex.Leg);
                ReplaceTransmogEquipment(hero.CivilianEquipment, EquipmentIndex.HorseHarness);
            }
            TransmogBehavior.transmogs_mesh = new Dictionary<ItemObject, ItemObject>();
            TransmogBehavior.transmogs_stat = new Dictionary<ItemObject, ItemObject>();
            InformationManager.DisplayMessage(new InformationMessage("All Transmog items reverted, MAKE A NEW SAVE before you uninstall!", Colors.Yellow));
        }

        static void ReplaceTransmogEquipment(Equipment equipment, EquipmentIndex index)
        {

            ItemObject transmog_item = equipment.GetEquipmentFromSlot(index).Item;
            if (transmog_item != null && transmog_item.StringId.StartsWith("TMOG_"))
            {
                string stat_id = transmog_item.StringId.Substring(5).Split(new string[] { "_TMOG_" }, StringSplitOptions.None)[0];
                foreach (ItemObject original_item in ItemObject.All.ToList())
                {
                    if (original_item.Id.ToString() == stat_id)
                    {
                        equipment.AddEquipmentToSlotWithoutAgent(index, new EquipmentElement(original_item, equipment.GetEquipmentFromSlot(index).ItemModifier));
                        break;
                    }
                }
                if (Traverse.Create(transmog_item).Property<bool>("isRegistered").Value)
                    Game.Current.ObjectManager.UnregisterObject(transmog_item);
                InformationManager.DisplayMessage(new InformationMessage("Replaced transmog equipment " + transmog_item.Name, Colors.Green));
            }
        }

        static void ReplaceTransmogItems(ItemRoster roster, HashSet<ItemObject> items_to_skip = null)
        {
            for (int i = roster.Count - 1; i >= 0; i--)
            {
                ItemObject transmog_item = roster[i].EquipmentElement.Item;
                if (transmog_item.StringId.StartsWith("TMOG_") && (items_to_skip == null || !items_to_skip.Contains(transmog_item)))
                {
                    string stat_id = transmog_item.StringId.Substring(5).Split(new string[] { "_TMOG_" }, StringSplitOptions.None)[0];
                    foreach (ItemObject original_item in ItemObject.All.ToList())
                    {
                        if (original_item.Id.ToString() == stat_id)
                        {
                            int amount = roster[i].Amount;
                            //roster.AddToCountsAtIndex(i, -amount);
                            roster.Remove(roster[i]);
                            roster.AddToCounts(original_item, amount);
                            break;
                        }
                    }
                    if (Traverse.Create(transmog_item).Property<bool>("isRegistered").Value)
                        Game.Current.ObjectManager.UnregisterObject(transmog_item);
                    InformationManager.DisplayMessage(new InformationMessage("Replaced transmog item " + transmog_item.Name, Colors.Green));
                }
            }
        }

        static void FindTransmogItems(ItemRoster roster, ref HashSet<ItemObject> transmogs)
        {
            foreach (ItemRosterElement element in roster)
            {
                ItemObject item = element.EquipmentElement.Item;
                if (item.StringId.StartsWith("TMOG_"))
                    transmogs.Add(item);
            }
        }

        static void FindTransmogItems(Hero hero, ref HashSet<ItemObject> transmogs)
        {
            FindEquipmentTransmog(hero.BattleEquipment, EquipmentIndex.Body, ref transmogs);
            FindEquipmentTransmog(hero.BattleEquipment, EquipmentIndex.Cape, ref transmogs);
            FindEquipmentTransmog(hero.BattleEquipment, EquipmentIndex.Gloves, ref transmogs);
            FindEquipmentTransmog(hero.BattleEquipment, EquipmentIndex.Head, ref transmogs);
            FindEquipmentTransmog(hero.BattleEquipment, EquipmentIndex.Leg, ref transmogs);
            FindEquipmentTransmog(hero.BattleEquipment, EquipmentIndex.HorseHarness, ref transmogs);
            FindEquipmentTransmog(hero.CivilianEquipment, EquipmentIndex.Body, ref transmogs);
            FindEquipmentTransmog(hero.CivilianEquipment, EquipmentIndex.Cape, ref transmogs);
            FindEquipmentTransmog(hero.CivilianEquipment, EquipmentIndex.Gloves, ref transmogs);
            FindEquipmentTransmog(hero.CivilianEquipment, EquipmentIndex.Head, ref transmogs);
            FindEquipmentTransmog(hero.CivilianEquipment, EquipmentIndex.Leg, ref transmogs);
            FindEquipmentTransmog(hero.CivilianEquipment, EquipmentIndex.HorseHarness, ref transmogs);
        }

        static void FindEquipmentTransmog(Equipment equipment, EquipmentIndex index, ref HashSet<ItemObject> transmogs)
        {
            ItemObject item = equipment.GetEquipmentFromSlot(index).Item;
            if (item != null && item.StringId.StartsWith("TMOG_"))
                transmogs.Add(item);
        }
    }
}
