﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using TaleWorlds.CampaignSystem.ViewModelCollection;
using TaleWorlds.Core;
using TaleWorlds.Library;

namespace Transmog
{
    class TransmogCopy
    {
        public static void SelectedItemCopy(ItemObject selected_item)
        {
            string clipboard_string;
            if (selected_item.StringId.StartsWith("TMOG_") && TransmogBehavior.transmogs_mesh[selected_item] == TransmogBehavior.transmogs_stat[selected_item])
            {
                clipboard_string = "<TransmogData " + selected_item.ItemType.ToString() + "=\"" + "hidden" + "\" />";
                InformationManager.DisplayMessage(new InformationMessage("Transmog copy: Hidden", Colors.Yellow));
            }
            else
            {
                while (selected_item.StringId.StartsWith("TMOG_"))
                    selected_item = TransmogBehavior.transmogs_mesh[selected_item]; // mesh_item is a transmogrified item, grab its original mesh_item
                clipboard_string = "<TransmogData " + selected_item.ItemType.ToString() + "=\"" + selected_item.StringId + "\" />";
                InformationManager.DisplayMessage(new InformationMessage("Transmog copy: " + selected_item.GetName(), Colors.Yellow));
            }
            System.Windows.Forms.Clipboard.SetText(clipboard_string);
            TransmogInventoryScreen.clipboard_item = selected_item;
        }

        public static void EquipmentInventoryCopy()
        {
            SPInventoryVM vm = TransmogInventoryScreen.GetSPInventoryVM();
            XElement element = new XElement("TransmogData");
            XmlAddItemObject(element, vm.CharacterHelmSlot.ItemRosterElement.EquipmentElement.Item);
            XmlAddItemObject(element, vm.CharacterCloakSlot.ItemRosterElement.EquipmentElement.Item);
            XmlAddItemObject(element, vm.CharacterTorsoSlot.ItemRosterElement.EquipmentElement.Item);
            XmlAddItemObject(element, vm.CharacterGloveSlot.ItemRosterElement.EquipmentElement.Item);
            XmlAddItemObject(element, vm.CharacterBootSlot.ItemRosterElement.EquipmentElement.Item);
            XmlAddItemObject(element, vm.CharacterMountArmorSlot.ItemRosterElement.EquipmentElement.Item);
            System.Windows.Forms.Clipboard.SetText(element.ToString());
            InformationManager.DisplayMessage(new InformationMessage("Transmog equipment copied!", Colors.Green));
        }

        public static void EquipmentPartyCopy(Equipment equipment)
        {
            XElement element = new XElement("TransmogData");
            XmlAddItemObject(element, equipment.GetEquipmentFromSlot(EquipmentIndex.Head).Item);
            XmlAddItemObject(element, equipment.GetEquipmentFromSlot(EquipmentIndex.Cape).Item);
            XmlAddItemObject(element, equipment.GetEquipmentFromSlot(EquipmentIndex.Body).Item);
            XmlAddItemObject(element, equipment.GetEquipmentFromSlot(EquipmentIndex.Gloves).Item);
            XmlAddItemObject(element, equipment.GetEquipmentFromSlot(EquipmentIndex.Leg).Item);
            XmlAddItemObject(element, equipment.GetEquipmentFromSlot(EquipmentIndex.HorseHarness).Item);
            System.Windows.Forms.Clipboard.SetText(element.ToString());

        }

        static void XmlAddItemObject(XElement element, ItemObject item)
        {
            if (item != null)
            {
                if (item.StringId.StartsWith("TMOG_") && TransmogBehavior.transmogs_mesh[item] == TransmogBehavior.transmogs_stat[item])
                    element.Add(new XAttribute(item.ItemType.ToString(), "hidden"));
                else
                {
                    item = Transmogrifier.FindOriginal(item, ref TransmogBehavior.transmogs_mesh);
                    element.Add(new XAttribute(item.ItemType.ToString(), item.StringId));
                }
            }
        }
    }
}
