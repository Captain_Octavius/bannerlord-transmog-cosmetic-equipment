﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using TaleWorlds.Core;
using TaleWorlds.Library;
using TaleWorlds.Localization;
using TaleWorlds.SaveSystem;

namespace Transmog
{
    class Transmogrifier
    {
        public static int TransmogItemTierDifferenceAllowed = 2;
        public static ItemObject TransmogItem(ItemObject stat_item, ItemObject mesh_item, bool hide_visuals = false)
        {
            TransmogBehavior.Initialize();
            if (stat_item.MultiMeshName == mesh_item.MultiMeshName && !hide_visuals)
                return null;
            if (!ValidateItems(stat_item, mesh_item))
            {
                InformationManager.DisplayMessage(new InformationMessage("Transmog not possible: Incompatible items.", Colors.Yellow));
                return null;
            }
            if (TransmogBehavior.transmogs_mesh.ContainsKey(stat_item) && TransmogBehavior.transmogs_mesh[stat_item] == mesh_item && TransmogBehavior.transmogs_stat[stat_item] != mesh_item)
            {
                InformationManager.DisplayMessage(new InformationMessage("Transmog is already applied: " + stat_item.Name.ToString(), Colors.Yellow));
                return null;
            }
            stat_item = FindOriginal(stat_item, ref TransmogBehavior.transmogs_stat);
            mesh_item = FindOriginal(mesh_item, ref TransmogBehavior.transmogs_mesh);
            if (hide_visuals)
                mesh_item = stat_item;
            if ((int)mesh_item.Tier - (int)stat_item.Tier > TransmogItemTierDifferenceAllowed)
            {
                InformationManager.DisplayMessage(new InformationMessage("Tier of " + stat_item.Name + " is too low for transmogrification to " + mesh_item.Name, Colors.Yellow));
                return null;
            }
            if (stat_item == mesh_item && !hide_visuals)
            {
                InformationManager.DisplayMessage(new InformationMessage("Transmog reverted: " + stat_item.Name.ToString(), Colors.Green));
                return stat_item;
            }
            string transmog_id = "TMOG_" + stat_item.Id.ToString() + "_TMOG_" + mesh_item.Id.ToString();
            bool already_exists = false;
            foreach (ItemObject list_item in ItemObject.All)
            {
                if (list_item.StringId == transmog_id)
                {
                    already_exists = true;
                    if (!hide_visuals)
                        InformationManager.DisplayMessage(new InformationMessage("Transmog " + stat_item.Name + " to " + mesh_item.Name, Colors.Green));
                    else
                        InformationManager.DisplayMessage(new InformationMessage("Transmog " + stat_item.Name + " to hidden!", Colors.Green));
                    return list_item;
                }
            }
            if (!already_exists)
            {
                ItemObject transmog_item = new ItemObject();
                transmog_item.StringId = transmog_id;
                CopyStats(transmog_item, stat_item, mesh_item);

                Game.Current.ObjectManager.RegisterObject<ItemObject>(transmog_item);
                MetaData metaData = null;
                Traverse.Create(transmog_item).Method("BeforeLoad", new Type[] { typeof(MetaData) }).GetValue(metaData);
                //transmog_item.BeforeLoad(null);
                transmog_item.AfterInitialized();

                TransmogBehavior.RegisterTransmogItem(transmog_item, stat_item, mesh_item);
                if (!hide_visuals)
                    InformationManager.DisplayMessage(new InformationMessage("Transmog " + stat_item.Name + " to " + mesh_item.Name, Colors.Green));
                else
                    InformationManager.DisplayMessage(new InformationMessage("Transmog " + stat_item.Name + " to hidden!", Colors.Green));
                return transmog_item;
            }
            return null;
        }

        public static ItemObject FindOriginal(ItemObject item, ref Dictionary<ItemObject, ItemObject> dictionary)
        {
            while (item.StringId.StartsWith("TMOG_"))
                item = dictionary[item];
            return item;
        }

        public static void CopyStats(ItemObject transmog_item, ItemObject stat_item, ItemObject mesh_item)
        {
            var item = Traverse.Create(transmog_item);
            item.Property<float>("Appearance").Value = stat_item.Appearance;
            item.Property<float>("Effectiveness").Value = stat_item.Effectiveness;
            item.Property<int>("LodAtlasIndex").Value = stat_item.LodAtlasIndex;
            item.Property<BasicCultureObject>("Culture").Value = stat_item.Culture;
            item.Property<ItemCategory>("ItemCategory").Value = stat_item.ItemCategory;
            item.Property<ItemComponent>("ItemComponent").Value = stat_item.ItemComponent;
            item.Property<string>("HolsterMeshName").Value = stat_item.HolsterMeshName;
            item.Property<string>("HolsterWithWeaponMeshName").Value = stat_item.HolsterWithWeaponMeshName;
            item.Property<string[]>("ItemHolsters").Value = stat_item.ItemHolsters;
            item.Property<Vec3>("HolsterPositionShift").Value = stat_item.HolsterPositionShift;
            item.Property<string>("FlyingMeshName").Value = stat_item.FlyingMeshName;
            item.Property<string>("BodyName").Value = stat_item.BodyName;
            item.Property<string>("HolsterBodyName").Value = stat_item.HolsterBodyName;
            item.Property<string>("CollisionBodyName").Value = stat_item.CollisionBodyName;
            item.Property<bool>("RecalculateBody").Value = stat_item.RecalculateBody;
            item.Property<string>("PrefabName").Value = stat_item.PrefabName;
            item.Property<int>("Value").Value = stat_item.Value;
            item.Property<float>("Weight").Value = stat_item.Weight;
            item.Property<int>("Difficulty").Value = stat_item.Difficulty;
            item.Property<string>("ArmBandMeshName").Value = stat_item.ArmBandMeshName;
            item.Property<bool>("IsFood").Value = stat_item.IsFood;
            item.Field<ItemObject.ItemTypeEnum>("Type").Value = stat_item.Type;
            item.Property<float>("ScaleFactor").Value = stat_item.ScaleFactor;
            //item.Property<TextObject>("Name").Value = copyFrom.Name;
            if (stat_item.Name != null)
                item.Property<TextObject>("Name").Value = new TextObject("<T> " + stat_item.Name.ToString());
            else
                item.Property<TextObject>("Name").Value = new TextObject("Transmog error: Item name not found!");
            item.Property<bool>("NotMerchandise").Value = true;

            ItemFlags flags = stat_item.ItemFlags;
            item.Property<bool>("IsUniqueItem").Value = false;
            if (mesh_item != stat_item)
            {
                item.Property<string>("MultiMeshName").Value = mesh_item.MultiMeshName;
                item.Property<bool>("IsUsingTableau").Value = mesh_item.IsUsingTableau;

                if (mesh_item.ItemFlags.HasFlag(ItemFlags.UseTeamColor))
                    flags |= ItemFlags.UseTeamColor; // enable UseTeamColor flag
                else
                    flags &= ~ItemFlags.UseTeamColor; // disable UseTeamColor flag
                if (mesh_item.ItemFlags.HasFlag(ItemFlags.NotUsableByFemale))
                    flags |= ItemFlags.NotUsableByFemale; // enable UseTeamColor flag
                else
                    flags &= ~ItemFlags.NotUsableByFemale; // disable UseTeamColor flag
                if (mesh_item.ItemFlags.HasFlag(ItemFlags.NotUsableByMale))
                    flags |= ItemFlags.NotUsableByMale; // enable UseTeamColor flag
                else
                    flags &= ~ItemFlags.NotUsableByMale; // disable UseTeamColor flag
                item.Property<ItemFlags>("ItemFlags").Value = flags;

                if (transmog_item.HasArmorComponent && mesh_item.HasArmorComponent)
                {
                    ArmorComponent new_component = (ArmorComponent)transmog_item.ArmorComponent.GetCopy();
                    item.Property<ItemComponent>("ItemComponent").Value = new_component;
                    var component = Traverse.Create(new_component);
                    component.Property<ItemModifierGroup>("ItemModifierGroup").Value = transmog_item.ArmorComponent.ItemModifierGroup;

                    ArmorComponent mesh_component = mesh_item.ArmorComponent;
                    component.Property<SkinMask>("MeshesMask").Value = mesh_component.MeshesMask;
                    component.Property<ArmorComponent.BodyMeshTypes>("BodyMeshType").Value = mesh_component.BodyMeshType;
                    component.Property<ArmorComponent.BodyDeformTypes>("BodyDeformType").Value = mesh_component.BodyDeformType;
                    component.Property<ArmorComponent.HairCoverTypes>("HairCoverType").Value = mesh_component.HairCoverType;
                    component.Property<ArmorComponent.BeardCoverTypes>("BeardCoverType").Value = mesh_component.BeardCoverType;
                    component.Property<ArmorComponent.HorseHarnessCoverTypes>("ManeCoverType").Value = mesh_component.ManeCoverType;
                    component.Property<ArmorComponent.ArmorMaterialTypes>("MaterialType").Value = mesh_component.MaterialType; //Appears to be used for sound hit effects only
                    component.Property<string>("ReinsMesh").Value = mesh_component.ReinsMesh;
                    component.Property<bool>("MultiMeshHasGenderVariations").Value = mesh_component.MultiMeshHasGenderVariations;
                }
            }
            else
            {
                item.Property<string>("MultiMeshName").Value = "none";
                item.Property<bool>("IsUsingTableau").Value = false;
                if (transmog_item.HasArmorComponent)
                {
                    ArmorComponent new_component = (ArmorComponent)transmog_item.ArmorComponent.GetCopy();
                    item.Property<ItemComponent>("ItemComponent").Value = new_component;
                    var component = Traverse.Create(new_component);
                    component.Property<ItemModifierGroup>("ItemModifierGroup").Value = transmog_item.ArmorComponent.ItemModifierGroup;

                    component.Property<SkinMask>("MeshesMask").Value = SkinMask.AllVisible;
                    component.Property<ArmorComponent.BodyMeshTypes>("BodyMeshType").Value = ArmorComponent.BodyMeshTypes.Normal;
                    component.Property<ArmorComponent.BodyDeformTypes>("BodyDeformType").Value = ArmorComponent.BodyDeformTypes.Medium;
                    component.Property<ArmorComponent.HairCoverTypes>("HairCoverType").Value = ArmorComponent.HairCoverTypes.None;
                    component.Property<ArmorComponent.BeardCoverTypes>("BeardCoverType").Value = ArmorComponent.BeardCoverTypes.None;
                    component.Property<ArmorComponent.HorseHarnessCoverTypes>("ManeCoverType").Value = ArmorComponent.HorseHarnessCoverTypes.None;
                    component.Property<ArmorComponent.ArmorMaterialTypes>("MaterialType").Value = ArmorComponent.ArmorMaterialTypes.None; //Appears to be used for sound hit effects only
                    component.Property<string>("ReinsMesh").Value = "none";
                    component.Property<bool>("MultiMeshHasGenderVariations").Value = false;
                }
            }
        }

        public static bool ValidateItems(ItemObject stat_item, ItemObject mesh_item)
        {
            if (
                stat_item.ItemType == mesh_item.ItemType &&
                //MobileParty.MainParty.ItemRoster.Contains(item.ItemRosterElement) && (
                stat_item.ItemType == ItemObject.ItemTypeEnum.BodyArmor ||
                stat_item.ItemType == ItemObject.ItemTypeEnum.Cape ||
                stat_item.ItemType == ItemObject.ItemTypeEnum.ChestArmor ||
                stat_item.ItemType == ItemObject.ItemTypeEnum.HandArmor ||
                stat_item.ItemType == ItemObject.ItemTypeEnum.HeadArmor ||
                stat_item.ItemType == ItemObject.ItemTypeEnum.LegArmor ||
                stat_item.ItemType == ItemObject.ItemTypeEnum.HorseHarness
                )
                return true;
            else
                return false;
        }

    }
}
